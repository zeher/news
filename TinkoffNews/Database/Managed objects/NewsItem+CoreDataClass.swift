//
//  NewsItem+CoreDataClass.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//
//

import Foundation
import CoreData

@objc(NewsItem)
public class NewsItem: NSManagedObject, Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "text"
        case date = "milliseconds"
        case type = "bankInfoTypeId"
        case content = "content"
        case publicationDateContainer = "publicationDate"
        case creationDateContainer = "creationDate"
        case titleContainer = "title"
    }
    
    required convenience public init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[.context] as? NSManagedObjectContext else { fatalError() }
        guard let entity = NSEntityDescription.entity(forEntityName: "NewsItem", in: context) else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        var titleContainer = container
        if let container = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .titleContainer) {
            titleContainer = container
        }
        
        let id = try titleContainer.decodeIfPresent(String.self, forKey: .id)
        if let id = id {
            self.id = id
        }
        
        let title = try titleContainer.decodeIfPresent(String.self, forKey: .title)
        if let title = title {
            self.title = title
        }
        
        let content = try container.decodeIfPresent(String.self, forKey: .content)
        if let content = content {
            self.content = content
        }
        
        let publicationContainer = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .publicationDateContainer)
        let creationContainer = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .creationDateContainer)
        let dateContainer = publicationContainer ?? creationContainer
        
        if let timeInterval = try? dateContainer?.decodeIfPresent(TimeInterval.self, forKey: .date), let interval = timeInterval {
            self.date = Date(timeIntervalSince1970: TimeInterval(interval / 1000))
        } else {
            throw SerrializationError.noData
        }
    }
}

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")!
}
