//
//  NewsItem+CoreDataProperties.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//
//

import Foundation
import CoreData

extension NewsItem {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<NewsItem> {
        return NSFetchRequest(entityName: "NewsItem")
    }

    @NSManaged public var id: String
    @NSManaged public var title: String
    @NSManaged public var date: Date
    @NSManaged public var type: Int32
    @NSManaged public var content: String?

}
