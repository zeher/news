//
//  ServiceAssembly.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 03/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import UIKit
import CoreData

class ServiceAssembly {
    
    static var shared = ServiceAssembly()
    
    let coreAssembly = CoreAssembly.shared
    
    lazy var feedService: NewsFeedService = {
        let feedService = NewsFeedBaseService()
        feedService.coreDataMapper = coreAssembly.coreDataMapper
        feedService.networkClient = coreAssembly.networkClient
        feedService.requestFactory = coreAssembly.requestFactory
        
        return feedService
    }()
    
    lazy var newsService: NewsService = {
        let newsService = NewsBaseService()
        newsService.coreDataMapper = coreAssembly.coreDataMapper
        newsService.networkClient = coreAssembly.networkClient
        newsService.requestFactory = coreAssembly.requestFactory
        return newsService
    }()
}
