//
//  NewsService.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

protocol NewsService {
    //синхронное получение закэшированных данных
    func obtainNews(with id: String) throws -> NewsItem
    
    //загрузка данных с сервера и сохранение в кэш
    func loadNews(id: String, completion: @escaping (() throws -> NewsItem?) -> ())
}
