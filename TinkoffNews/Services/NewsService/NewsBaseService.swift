//
//  NewsBaseService.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation
import CoreData

final class NewsBaseService: NewsService {
    var networkClient: NetworkClient!
    var requestFactory: RequestFactory!
    var coreDataMapper: CoreDataMapper!
    
    func obtainNews(with id: String) throws -> NewsItem {
        let fetchRequest: NSFetchRequest<NewsItem> = NewsItem.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        let items: NewsItem = try coreDataMapper.fetch(fetchRequest)
        return items
    }
    
    func loadNews(id: String, completion: @escaping (() throws -> NewsItem?) -> ()) {
        let request = requestFactory.newsRequest(with: id)
        networkClient.performRequest(request) { result in
            let closure = { [weak self] () -> NewsItem? in
                guard let sSelf = self else { return nil }
                
                switch result {
                case let .data(data):
                    try sSelf.mapAndSave(data, id: id)
                    return try sSelf.obtainNews(with: id)
                case let .error(error):
                    throw error
                }
            }
            
            completion(closure)
        }
    }
    
    func mapAndSave(_ data: Data, id: String) throws {
        let predicate = NSPredicate(format: "id == %@", id)
        try coreDataMapper.deleteBatch(NewsItem.self, predicate: predicate)
        try coreDataMapper.map(JSONResponse<NewsItem>.self, data: data)
    }

}
