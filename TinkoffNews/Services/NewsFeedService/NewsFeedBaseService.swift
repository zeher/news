//
//  NewsFeedBaseService.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 03/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation
import CoreData

final class NewsFeedBaseService: NewsFeedService {
    
    var networkClient: NetworkClient!
    var requestFactory: RequestFactory!
    var coreDataMapper: CoreDataMapper!
    
    func obtainNewsFeed() throws -> [NewsItem] {
        let fetchRequest: NSFetchRequest<NewsItem> = NewsItem.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.fetchBatchSize = 20
        fetchRequest.returnsObjectsAsFaults = false
        
        let items: [NewsItem] = try coreDataMapper.fetch(fetchRequest)
        
        return items
    }
    
    func loadNewsFeed(completion: @escaping (() throws -> [NewsItem]) -> ()) {
        let request = requestFactory.feedRequest()
        networkClient.performRequest(request) { result in
            let closure = { [weak self] () -> [NewsItem] in
                guard let sSelf = self else { return [] }
                
                switch result {
                case let .data(data):
                    try sSelf.mapAndSave(data)
                    return try sSelf.obtainNewsFeed()
                case let .error(error):
                    throw error
                }
            }
            completion(closure)
        }
    }
    
    func mapAndSave(_ data: Data) throws {
        //Здесь по хорошему нужно не удалять и создавать заново, а обновлять данные в базе. Но я заюзала Decodable для маппинга, поэтому это стало нетривиальной задачей
        try coreDataMapper.deleteBatch(NewsItem.self)
        try coreDataMapper.map(JSONResponse<[NewsItem]>.self, data: data)
    }
}
