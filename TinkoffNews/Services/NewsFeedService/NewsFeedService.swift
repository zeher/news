

//
//  NewsFeedService.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

protocol NewsFeedService {
    
    //синхронное получение закэшированных данных
    func obtainNewsFeed() throws -> [NewsItem]
    
    //загрузка данных с сервера и сохранение в кэш
    func loadNewsFeed(completion: @escaping (() throws -> [NewsItem]) -> ())
}

