//
//  URLSessionNetworkClient.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

enum NetworkClientError: Error {
    case unknown
}

final class URLSessionNetworkClient: NetworkClient {
    
    func performRequest(_ request: URLRequest, completion: @escaping (Result) -> ()) {
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.error(error))
                return
            }
            if let data = data {
                completion(.data(data))
                return
            }
            
            completion(.error(NetworkClientError.unknown))
        }
        
        task.resume()
    }
}
