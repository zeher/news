//
//  RequestFactory.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

protocol RequestFactory {
    
    func feedRequest() -> URLRequest
    
    func newsRequest(with id: String) -> URLRequest
}
