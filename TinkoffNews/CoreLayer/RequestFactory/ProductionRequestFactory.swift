//
//  ProductionRequestFactory.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

private struct APIEndpoints {
    static let baseURL = URL(string:"https://api.tinkoff.ru/v1/")!
    static let newsFeed = URL(string: "news", relativeTo: baseURL)!
    static let newsDetails = URL(string: "news_content", relativeTo: baseURL)!
}

//Здесь можно настраивать url request-ы (политику кэширования, credentials, timeout и тд)
final class ProductionRequestFactory: RequestFactory {
    
    func feedRequest() -> URLRequest {
        let urlRequest = URLRequest(url: APIEndpoints.newsFeed)
        
        return urlRequest
    }
    
    func newsRequest(with id: String) -> URLRequest {
        let queryItem = URLQueryItem(name: "id", value: id)
        var urlComponents = URLComponents(url: APIEndpoints.newsDetails, resolvingAgainstBaseURL: true)!
        urlComponents.queryItems = [queryItem]
        let request = URLRequest(url: urlComponents.url!)
        
        return request
    }
}
