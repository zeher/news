//
//  JSONResponse.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

struct JSONResponse<T: Decodable> {
    let payload: T
} 

extension JSONResponse: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case payload = "payload"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.decode(T.self, forKey: .payload)
        
        self.init(payload: data)
    }
}

enum SerrializationError: Error {
    case noData
}
