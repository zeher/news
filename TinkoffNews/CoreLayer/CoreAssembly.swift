//
//  CoreAssembly.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 03/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import UIKit

class CoreAssembly {
    
    static var shared: CoreAssembly = CoreAssembly()
    
    var networkClient: NetworkClient {
        return URLSessionNetworkClient()
    }
    
    var requestFactory: RequestFactory {
        return ProductionRequestFactory()
    }
    
    var coreDataMapper: CoreDataMapper {
        let mapper = CoreDataMapper()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        mapper.container = appDelegate.persistentContainer
        return mapper
    }
}
