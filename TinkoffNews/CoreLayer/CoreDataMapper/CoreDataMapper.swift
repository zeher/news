//
//  CoreDataMapper.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation
import CoreData

enum FetchError: Error {
    case noData
}

final class CoreDataMapper {
    var container: NSPersistentContainer!
    
    func fetch<T: NSManagedObject>(_ fetchRequest: NSFetchRequest<T>) throws -> T {
        let item = try container.viewContext.fetch(fetchRequest)
        if let news = item.first {
            return news
        } else {
            throw FetchError.noData
        }
    }
    
    func fetch<T: NSManagedObject>(_ fetchRequest: NSFetchRequest<T>) throws -> [T] {
        let items = try container.viewContext.fetch(fetchRequest)
        return items
    }
    
    func deleteBatch<T: NSManagedObject>(_ type: T.Type, predicate: NSPredicate? = nil) throws {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = T.fetchRequest()
        fetchRequest.predicate = predicate
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        let context = self.container.newBackgroundContext()
        try context.execute(deleteRequest)
    }
    
    func map<T: Decodable>(_ type: T.Type, data: Data) throws {
        let decoder = JSONDecoder()
        let context = container.newBackgroundContext()
        decoder.userInfo[.context] = context
        
        _ = try decoder.decode(T.self, from: data)
        try context.save()
    }
}
