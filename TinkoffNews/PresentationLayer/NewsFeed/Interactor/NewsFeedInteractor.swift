//
//  NewsFeedInteractor.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

final class NewsFeedInteractor: NewsFeedInteractorInput {
    var output: NewsFeedInteractorOutput!
    var feedService: NewsFeedService!
    
    func getCachedFeed() -> [NewsItem] {
        //Здесь вместо try? нужно делать do/catch, тогда получим возможность обработать определенным образом конкретные ошибки
        let items = try? feedService.obtainNewsFeed()
        return items ?? []
    }
    
    func loadFeed() {
        feedService.loadNewsFeed { [weak self] result in
            guard let sSelf = self else { return }
            //и здесь
            let result = try? result()
            
            DispatchQueue.main.async {
                if let result = result {
                    sSelf.output.didLoadFeed(items: result)
                } else {
                    sSelf.output.didFailLoad()
                }
            }
        }
    }
}

