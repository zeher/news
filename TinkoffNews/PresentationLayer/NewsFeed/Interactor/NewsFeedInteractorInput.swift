//
//  NewsFeedInteractorInput.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

protocol NewsFeedInteractorInput {
    func getCachedFeed() -> [NewsItem]
    
    func loadFeed()
}
