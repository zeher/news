//
//  NewsFeedAssembly.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

final class NewsFeedAssembly {
    
    static var shared = NewsFeedAssembly()
    
    let serviceAssembly = ServiceAssembly.shared
    
    func inject(to controller: NewsFeedViewController) {
        let presenter = NewsFeedPresenter()
        
        let tableController = NewsFeedTableController()
        controller.output = presenter
        controller.tableController = tableController

        let interactor = NewsFeedInteractor()
        let feedService = serviceAssembly.feedService
        interactor.output = presenter
        interactor.feedService = feedService

        
        let router = NewsFeedRouter()
        router.transitionHandler = controller

        let mapper = NewsFeedModelMapper()
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        mapper.dateFormatter = formatter

        presenter.view = controller
        presenter.interactor = interactor
        presenter.mapper = mapper
        presenter.router = router
    }
}
