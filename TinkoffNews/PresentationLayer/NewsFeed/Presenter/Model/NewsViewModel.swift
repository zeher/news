//
//  NewsModel.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

struct NewsViewModel {
    let id: String
    let title: String
    let date: String
    
    let selectAction: (String) -> ()
}
