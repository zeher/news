//
//  NewsFeedPresenter.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

final class NewsFeedPresenter {
    var view: NewsFeedViewInput!
    var interactor: NewsFeedInteractorInput!
    var router: NewsFeedRouterInput!
    var mapper: NewsFeedModelMapper!
}

extension NewsFeedPresenter: NewsFeedViewOutput {
    func viewIsReady() {
        let cached = interactor.getCachedFeed()
        let viewModels = mapper.map(cached, actionHandler: self)
        view.update(with: viewModels)
        
        interactor.loadFeed()
    }
    
    func didTriggerRefresh() {
        interactor.loadFeed()
    }
}

extension NewsFeedPresenter: NewsFeedInteractorOutput {
    
    func didLoadFeed(items: [NewsItem]) {
        let viewModels = mapper.map(items, actionHandler: self)
        view.update(with: viewModels)
    }
    
    func didFailLoad() {
        view.showErrorView()
    }
}

extension NewsFeedPresenter: NewsViewModelActionHandler {
    func didSelect(with id: String) {
        router.showDetailNews(with: id)
    }
}
