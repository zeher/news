//
//  NewsFeedModelMapper.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

protocol NewsViewModelActionHandler {
    func didSelect(with id: String)
}

final class NewsFeedModelMapper {
    var dateFormatter: DateFormatter!
    
    func map(_ objects: [NewsItem], actionHandler: NewsViewModelActionHandler) -> [NewsViewModel] {
        let viewModels: [NewsViewModel] = objects.map { item in
            let dateString = dateFormatter.string(from: item.date as Date)
            return NewsViewModel(id: item.id, title: item.title, date: dateString) { id in
                actionHandler.didSelect(with: id)
            }
        }
        
        return viewModels
    }
}
