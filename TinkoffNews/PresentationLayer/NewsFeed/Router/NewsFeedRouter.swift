//
//  Router.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import UIKit

final class NewsFeedRouter: NewsFeedRouterInput {
    
    var transitionHandler: UIViewController!
    
    func showDetailNews(with id: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "NewsDetailsViewController")
        if let newsController = controller as? NewsDetailsViewController, let moduleInput = newsController.output as? NewsDetailsModuleInput {
            moduleInput.configure(with: id)
        }
        
        transitionHandler.show(controller, sender: nil)
    }
}
