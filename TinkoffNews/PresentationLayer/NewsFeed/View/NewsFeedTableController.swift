//
//  NewsFeedTableController.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import UIKit

final class NewsFeedTableController: NSObject {
    
    private var models: [NewsViewModel] = []
    
    func configure(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "NewsFeedTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "NewsFeedTableViewCell")
    }
    
    func update(_ items: [NewsViewModel]) {
        models = items
    }
}

extension NewsFeedTableController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = models[indexPath.row]
        model.selectAction(model.id)
    }
}

extension NewsFeedTableController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedTableViewCell", for: indexPath)
        
        guard let newsFeedCell = cell as? NewsFeedTableViewCell else { return cell }
        
        newsFeedCell.configure(models[indexPath.row])
        
        return newsFeedCell
    }
}
