//
//  NewsFeedTableViewCell.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 03/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import UIKit

class NewsFeedTableViewCell: UITableViewCell {

      @IBOutlet var title: UILabel!
      @IBOutlet var date: UILabel!
    
    func configure(_ model: NewsViewModel) {
        title.text = model.title
        date.text = model.date
    }
    
}
