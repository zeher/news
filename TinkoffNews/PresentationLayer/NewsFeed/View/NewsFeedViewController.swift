//
//  ViewController.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 01/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import UIKit

final class NewsFeedViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    var output: NewsFeedViewOutput!
    var tableController: NewsFeedTableController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableController.configure(tableView)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.addSubview(self.refreshControl)

        output.viewIsReady()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NewsFeedAssembly.shared.inject(to: self)
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        
        return refreshControl
    }()
    
    @objc func refreshTableView(refreshControl: UIRefreshControl) {
        output.didTriggerRefresh()
    }
}

extension NewsFeedViewController: NewsFeedViewInput {
    func update(with items: [NewsViewModel]) {
        tableController.update(items)
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func showErrorView() {
        refreshControl.endRefreshing()
        errorView.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] () in
            self?.errorView.isHidden = true
        }
    }
}

