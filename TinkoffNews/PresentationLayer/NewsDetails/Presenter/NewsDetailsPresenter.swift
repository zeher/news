//
//  NewsDetailsPresenter.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

final class NewsDetailsPresenter: NewsDetailsModuleInput {
    var interactor: NewsDetailsInteractorInput!
    var view: NewsDetailsViewInput!
    
    var newsID: String?
    
    func configure(with id: String) {
        newsID = id
    }
}

extension NewsDetailsPresenter: NewsDetailsViewOutput {
    func viewIsReady() {
        guard let id = newsID else { return }
        
        let cached = interactor.getCachedNews(for: id)
        if let html = cached?.content, let title = cached?.title {
            view.update(with: html, title: title)
        }
        
        interactor.loadNews(with: id)
    }
}

extension NewsDetailsPresenter: NewsDetailsInteractorOutput {
    func didLoadNews(newsItem: NewsItem) {
        if let html = newsItem.content {
            view.update(with: html, title: newsItem.title)
        }
    }
    
    func didFailLoad() {
        view.showErrorView()
    }
}
