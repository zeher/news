//
//  NewsDetailsViewInput.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

protocol NewsDetailsViewInput {    
    func update(with html: String, title: String)
    
    func showErrorView()
}
