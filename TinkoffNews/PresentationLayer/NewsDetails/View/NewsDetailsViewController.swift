//
//  NewsDetailsViewController.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import UIKit
import WebKit

final class NewsDetailsViewController: UIViewController {
    
    var output: NewsDetailsViewOutput!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NewsDetailsAssembly.shared.inject(to: self)
    }
}

extension NewsDetailsViewController: NewsDetailsViewInput {
    
    func update(with html: String, title: String) {
        errorView.isHidden = true
        navigationItem.title = title
        webView.loadHTMLString(html, baseURL: nil)
    }
    
    func showErrorView() {
        if webView.isLoading {
            return
        }
        errorView.isHidden = false
    }
}
