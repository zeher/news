//
//  NewsDetailsInteractorInput.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

protocol NewsDetailsInteractorInput {
    func getCachedNews(for id: String) -> NewsItem?
    
    func loadNews(with id: String)
}
