//
//  NewsDetailsInteractor.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

final class NewsDetailsInteractor: NewsDetailsInteractorInput {
    var output: NewsDetailsInteractorOutput!
    var newsService: NewsService!
    
    func getCachedNews(for id: String) -> NewsItem? {
        let item = try? newsService.obtainNews(with: id)
        return item
    }
    
    func loadNews(with id: String) {
        newsService.loadNews(id: id) { [weak self] result in
            guard let sSelf = self else { return }

            let results = try? result()
            DispatchQueue.main.async {
                if case let item?? = results {
                    sSelf.output.didLoadNews(newsItem: item)
                } else {
                    sSelf.output.didFailLoad()
                }
            }
        }
    }
}
