//
//  NewsDetailsAssembly.swift
//  TinkoffNews
//
//  Created by Vasyura Anastasiya on 04/12/2017.
//  Copyright © 2017 Stalin&Co. All rights reserved.
//

import Foundation

class NewsDetailsAssembly {
    
    static var shared = NewsDetailsAssembly()
    
    let serviceAssembly: ServiceAssembly = ServiceAssembly.shared
    
    func inject(to controller: NewsDetailsViewController) {
        let presenter = NewsDetailsPresenter()
        
        controller.output = presenter
        
        let interactor = NewsDetailsInteractor()
        let newsService = serviceAssembly.newsService
        interactor.output = presenter
        interactor.newsService = newsService

        presenter.view = controller
        presenter.interactor = interactor
    }
}
